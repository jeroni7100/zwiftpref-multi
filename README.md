# zwift-preferences

**ZwiftPref - Change world and other settings**


## To Use

* Install
* Run

prefs.xml is loaded when the app starts and when you press the 'Reload' button.

Change settings as you like and press 'Save' to have the changes written to prefs.xml.

If you close the app without pressing 'Save' nothing is changed in prefs.xml




## License
[CC0 1.0 (Public Domain)](LICENSE.md)

This license applies only to the original parts of the work. The original licenses of any included packages apply without modification.


## History

#### 0.3.0 2018-03-21

Fully data driven listing of worlds and routes (preparing for a new world)
Remember and recall preferred monitor (stores in saved_prefs.json)

#### 0.1.1 2017-01-26

* All platforms:
** xxx


## Acknowledgements

### Built on

* [electron](http://electron.atom.io)
* [node.js](http://nodejs.org)
* [photon](http://photonkit.com/)

### Built with

* [electron-packager](https://www.npmjs.com/package/electron-packager)
* [electron-installer-dmg](https://www.npmjs.com/package/electron-installer-dmg)
* [inno-setup]
r