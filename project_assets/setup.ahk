; $Date: 2017/01/16 15:30:43 $

#SingleInstance force

#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

AppName := "ZwiftPref"


; Step 1 : Compile 
; Step 2 : Update .iss
; Step 3 : Build packages

; Step 1 -----------------------------


; Step 2 -----------------------------

SetupISS := % A_ScriptDir "\setup.iss"

FileRead, PackageJSON, % A_ScriptDir "\..\package.json"
FileRead, ISSFile, % SetupISS

; msgbox, % PackageJSON
; msgbox, % ISSFile

FoundPosVer := RegExMatch(PackageJSON, "imU)\""version\""\s*:\s*\""(?P<Ver>[\.\d]+)\""", Match)
;msgbox, % Match
;msgbox, % MatchVer

NewMAV := MatchVer 

FoundPosMAV := RegExMatch(ISSFile, "im)#define\s+MyAppVersion\s+\""(?P<MAV>[\.\d]+)\""", Match)
;msgbox, % MatchMAV

NewISSFile := StrReplace(ISSFile, MatchMav, NewMav)
;msgbox, % NewISSFile

FoundPosMAV := RegExMatch(NewISSFile, "im)#define\s+MyAppVersion\s+\""(?P<MAV>[\.\d]+)\""", Match)
;msgbox, % MatchMAV

FileDelete, % SetupISS
FileAppend, % NewISSFile, % SetupISS


; Step 3 -----------------------------




return

