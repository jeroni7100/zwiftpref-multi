// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.


const app = require('electron').remote.app
const fs = require('fs')
var xml2js = require('xml2js');
const opn = require('opn');
var rp = require('request-promise-native');
var moment = require('moment');
const is = require('electron-is');

const hasFlag = require('electron').remote.require('has-flag')


prefsxml = app.getPath('documents') + "/Zwift/prefs.xml";
savedprefsjson = app.getPath('documents') + "/Zwift/saved_prefs.json";
routesjson = __dirname + '/routes.json';
if (hasFlag('test')) {
  routesjson = __dirname + '/routes-test.json';
}
routesjsonurl = 'https://api.zwifthacks.com/zwiftpref/json/routes.json';
// mapscheduleurl = 'http://cdn.zwift.com/gameassets/MapSchedule.xml'
mapscheduleurl = 'http://cdn.zwift.com/gameassets/MapSchedule_v2.xml'
// eventsjsonurl = 'https://zwift.com/api/v2.5/events/'
eventsjsonurl = 'https://zwifthacks.com/cache/events-zml.json'

fs.access(prefsxml, fs.constants.F_OK, (err) => {
  if (err == null) {
    //code when all ok
  } else {
    // STOP!!!
    throw (err)
  }
})


var parser = new xml2js.Parser({explicitChildren: false});
var builder = new xml2js.Builder({headless: true});

var prefs; // global var referenced in storePrefsPromise
readPrefsPromise().then(storePrefsPromise).then(showPrefs)

var savedprefs; // global var referenced in storePrefsPromise
readSavedPrefsPromise().then(showSavedPrefs)

var worlds; // global var 
var worldData = []; // global var

var buttonSave = document.querySelector('#button-save');
var buttonReload = document.querySelector('#button-reload');
var inputTrainerEffect = document.querySelector('#trainer-effect')

buttonSave.addEventListener('click', () => {
  savePrefs();
  saveSavedPrefs();
})

buttonReload.addEventListener('click', () => {
    readPrefsPromise().then(storePrefsPromise).then(showPrefs)
    readSavedPrefsPromise().then(showSavedPrefs)
})

inputTrainerEffect.addEventListener('input', () => {
  document.querySelector('#display-trainer-effect').innerHTML = (inputTrainerEffect.value * 100).toFixed(0) + '%'
})

document.querySelector('#button-world').addEventListener('click', () => {
  activateWindowContent('#window-content-world')
})

document.querySelector('#button-trainer').addEventListener('click', () => {
  activateWindowContent('#window-content-trainer')
})

document.querySelector('#button-misc').addEventListener('click', () => {
  activateWindowContent('#window-content-misc')
})

document.querySelector('#button-info').addEventListener('click', () => {
  activateWindowContent('#window-content-info')
})

document.querySelector('#button-zwifthacks').addEventListener('click', () => {
  opn('https://zwifthacks.com');
})

document.querySelector('#button-my-zwift').addEventListener('click', () => {
  opn('https://my.zwift.com/settings/profile');
})

document.querySelector('#button-show-prefs').addEventListener('click', () => {
    if (is.windows()) {
      opn(prefsxml, {app: ['notepad']});
    } else if (is.macOS()) {
      opn(prefsxml, {app: ['textedit']});
    }
})

document.querySelector('#button-remember-monitor').addEventListener('click', () => {
  rememberMonitor();
})

document.querySelector('#button-recall-monitor').addEventListener('click', () => {
  recallMonitor();
})

// routes

// console.log('dirname: ' + __dirname)

// next events

readEventsPromise().then(showEvents);


// Function definitions after this point ***************

function readSchedulePromise () {
  return new Promise( (resolve, reject) => {
    rp({uri: mapscheduleurl, timeout: 2000 })
      .then( (res) => {
        resolve(parseStringPromise(res))
      })
      .catch( (err) => {
        ' ignore'
      })

  })
}

function showCurrentWorld(schedule) {
  // alert(schedule)
  // console.log(schedule)
  start = schedule['MapSchedule']['appointments'][0]['appointment'][0]['$']['start'];
  // console.log(start)
  // console.log(moment(start).format())
  // console.log(moment().format())

  var currentStart = '1900-01-01T00:00-00';
  var currentWorld = '';

  try {
    schedule['MapSchedule']['appointments'][0]['appointment'].forEach( (appointment) => {
      // console.log(moment(appointment['$']['start']).format())
      appMoment = moment(appointment['$']['start'])
      if (moment().isSameOrAfter(appMoment) && appMoment.isSameOrAfter(moment(currentStart))) {
        currentStart = appointment['$']['start'];
        currentWorld = appointment['$']['map']
      }
    })
  } catch (error) {
    // ignore
  }

  document.querySelector('#current-world').innerHTML = (currentWorld == '' ? '' : 'Calendar world: ' + currentWorld);
}

function readRoutesPromise () {
  return new Promise( (resolve, reject) => {
      if (hasFlag('offline')) {
        fs.readFile(routesjson, 'utf8', (err, data) => {
          // if (err) throw err; // we'll not consider error handling for now
          if (err) reject(err); 
          else resolve(worlds = JSON.parse(data));
        });
      } else {
        rp({uri: routesjsonurl, timeout: 1500, json: true})
        .then( (res) => {
          // alert(res);
          resolve(worlds = res)
        })
        .catch( (err) => {
          // alert(err)
          fs.readFile(routesjson, 'utf8', (err, data) => {
            // if (err) throw err; // we'll not consider error handling for now
            if (err) reject(err); 
            else resolve(worlds = JSON.parse(data));
          });
        })
      }
  });
}

function showRoutes (worlds) {
    // console.log('Worlds:')

    document.querySelector('#route-selection').innerHTML = ''

    worlds.forEach( (w) => {

      // console.dir(w)
      // console.log(w.world, w.worldName)
      
      var e = document.createElement("div")
      document.querySelector('#route-selection').appendChild(e)
      renderWorldRoutes(e, w.world, w.worldName, w.worldDisplayName, w.routes, 'Cycling')

    })

    worlds.forEach( (w) => {

      var e = document.createElement("div")
      document.querySelector('#route-selection').appendChild(e)
      renderWorldRoutes(e, w.world, w.worldName, w.worldDisplayName, w.routes, 'Running')

    })
}

function storeWorldData (worlds) {
  // console.log('Worlds:')

  worlds.forEach( (w) => {

    // save worldData for later use
    worldData[w.world] = w
  })
}

function showWorlds (worlds) {
  // console.log('Worlds:')

  document.querySelector('#world-selection').innerHTML = ''

  worlds.forEach( (w) => {

    // console.dir(w)
    // console.log(w.world, w.worldName)

    var e = document.createElement("div")
    let res = w.prefs.prefsDisplayName.split ( /(.*)&(.)(.*)/i )

    document.querySelector('#world-selection').appendChild(e)
    e.outerHTML = '<div class="radio"><label><input id="' + w.prefs.prefsId + '" type="radio" value="' + w.world + '" name="world" accesskey="' + w.prefs.prefsAccessKey + '" />' + res[1] + '<span class="accesskey">' + res[2] + '</span>' + res[3] + '</label></div>'
  })
}

function readEventsPromise () {
  return new Promise( (resolve, reject) => {
      if (hasFlag('offline')) {
        resolve({events: []});
      } else {
        rp({uri: eventsjsonurl, timeout: 3000, json: true})
          .then( (res) => {
            // console.log(res);
            // resolve(res.events.sort((a,b) => parseInt(a.start_time) - parseInt(b.start_time)))
            resolve(res.filter( (e) => {
              // console.log(moment(e.eventStart, "Y-M-DTH:m:s.SZ"))
              return moment().isSameOrBefore(moment(e.eventStart, "Y-M-DTH:m:s.SZ"))
            }))
          })
          .catch( (err) => {
            console.log(err)
            resolve({events: []});
          })
      }
  });
}

function showEvents (events) {

    console.log(events)
    document.querySelector('#events').innerHTML = ''

    if (events.length > 0) {
      i = 1;
  
      events.some( (e) => {
        var d = document.createElement("div")
        document.querySelector('#events').appendChild(d)
        renderEvent(d, e.name, e.id, e.eventStart)
        return (i++ >= 4) 
      })
    }
}

function renderEvent(parentNode, name, id, start_time) {

    var d = document.createElement("div")
    parentNode.appendChild(d)

    d.outerHTML = '<div class="event" id="event_' + id + '"><div class="icon"><i class="fa fa-clock-o" aria-hidden="true"></i></div><div class="event-start">' + moment(start_time, "Y-M-DTH:m:s.SZ").format("LT") + '</div><div class="icon"> </div><div class="event-name">' + name +' &nbsp;&nbsp;<a href="#" onclick="require(\'opn\')(\'http://zwift.com/events/view/' + id + '\')"><i class="fa fa-external-link" aria-hidden="true"></i></a></div>'
}


function parseStringPromise(string)
{
    return new Promise( (resolve, reject) =>
    {
        parser.parseString(string, (err, result) => {
            if (err) {
                return reject(err);
             } else {
                return resolve(result);
             }
        });
    });
} // parseStringPromise

function readPrefsPromise () {
  return new Promise( (resolve, reject) => {
      fs.readFile(prefsxml, function (err, data) {
        // if (err) throw err; // we'll not consider error handling for now
        if (err) reject(err); // we'll not consider error handling for now
        else resolve(parseStringPromise(data))
      });
  });
} // readPrefsPromise

function storePrefsPromise (data) {
  return Promise.resolve(prefs = data); // sets global prefs var
} // storePrefsPromise

function showPrefs (prefs) {
  // console.log(prefs)

  // World

  world = getNumberPref('WORLD', '', 0)
  
  // document.querySelector('#current_world').textContent = world
  
  if (!hasFlag('offline')) {
    readSchedulePromise().then(showCurrentWorld)
  }

  // // Branch Preferences

  // // xyzbranch = getNumberPref('CONFIG', 'XYZ_BRANCH_PREFERENCE')
  // londonbranch = getNumberPref('CONFIG', 'LONDON_BRANCH_PREFERENCE')
  // richmondbranch = getNumberPref('CONFIG', 'RICHMOND_BRANCH_PREFERENCE')
  // watopiabranch = getNumberPref('CONFIG', 'BRANCH_PREFERENCE')

  // // xyzroute = getNumberPref('CONFIG', 'XYZ_ROUTE_SIGNATURE')
  // londonroute = getNumberPref('CONFIG', 'LONDON_ROUTE_SIGNATURE')
  // richmondroute = getNumberPref('CONFIG', 'RICHMOND_ROUTE_SIGNATURE')
  // watopiaroute = getNumberPref('CONFIG', 'ROUTE_SIGNATURE')

  readRoutesPromise().then((wrlds) => {
    storeWorldData(wrlds);
    showWorlds(wrlds);
    showRoutes(wrlds);

    var choices = document.getElementsByName('world')
    for (var i = 0; i < choices.length; i++) {
      if (choices[i].value == world) {
        // console.log('Will check value ' + world + ' (' + choices[i].id + ')' )
        choices[i].checked = true
      } else {
        choices[i].checked = false
      }
    }

  });

  // Trainer Effect

  // console.dir(prefs)
  trainereffect = getNumberPref('CONFIG', 'TRAINER_EFFECT', 0.5)
  // console.log('trainereffect is ' + trainereffect)
  
  document.querySelector('#trainer-effect').value = trainereffect
  document.querySelector('#display-trainer-effect').innerHTML =  (inputTrainerEffect.value * 100).toFixed(0) + '%'


  // Neo Road Feel
  
  neoroadfeel = getBooleanPref('CONFIG', 'NEOROADFEEL', false)
  // console.log('neoroadfeel is ' + neoroadfeel)
  
  document.querySelector('#roadfeel').checked = neoroadfeel

  //

  sport = getStringPref('SPORT', '', 'CYCLING')
  // console.log(sport)
  var choices = document.getElementsByName('sport')
  for (var i = 0; i < choices.length; i++) {
    if (choices[i].value == sport) {
      // console.log('Will check value ' + world + ' (' + choices[i].id + ')' )
      choices[i].checked = true
    } else {
      choices[i].checked = false
    }
  }

  // 

  minimalui = getBooleanPref('CONFIG', 'MINIMAL_UI', false)
  //  Default value (if node not present) is 1 !!
  minimalleaderboards = getBooleanPref('CONFIG', 'MINIMAL_LEADERBOARDS', true)
  fullscreen = getBooleanPref('CONFIG', 'FULLSCREEN', false)
  editinwatts = getBooleanPref('WORKOUTS', 'EDIT_IN_WATTS', false)
  erg = getBooleanPref('WORKOUTS', 'USE_ERG', false)
  powersmoothing = getBooleanPref('CONFIG', 'POWERSMOOTHING', false)
  
  document.querySelector('#minimal-ui').checked = minimalui
  document.querySelector('#minimal-leaderboards').checked = minimalleaderboards
  document.querySelector('#fullscreen').checked = fullscreen
  document.querySelector('#edit-in-watts').checked = editinwatts
  document.querySelector('#use-erg').checked = erg
  document.querySelector('#power-smoothing').checked = powersmoothing
  
  //
  
  preferred_monitor = getNumberPref('CONFIG', 'PREFERRED_MONITOR');
  // remembered_monitor = getNumberPref('ZWIFTPREF_REMEMBER', 'PREFERRED_MONITOR');

  document.querySelector('#preferred-monitor').textContent = preferred_monitor;
  // document.querySelector('#remembered-monitor').textContent = remembered_monitor;
  
} // showPrefs


function readSavedPrefsPromise () {
  return new Promise( (resolve, reject) => {
      fs.readFile(savedprefsjson, 'utf8', function (err, data) {
        // if (err) throw err; // we'll not consider error handling for now
        if (err) {
          //reject(err); 
          // we'll not consider error handling for now
          data = '{"saved":[]}';
        }
        // console.log(data);
        resolve(savedprefs = JSON.parse(data));
      });
  });
} // readPrefsPromise

function showSavedPrefs(savedprefs) {
  // console.log(savedprefs)

  // preferred_monitor = getNumberPref('CONFIG', 'PREFERRED_MONITOR');
  try {
    remembered_monitor = savedprefs['saved'][0]['CONFIG']['PREFERRED_MONITOR']
  }
  catch(err) {
    remembered_monitor = ''
  }
  // getNumberPref('ZWIFTPREF_REMEMBER', 'PREFERRED_MONITOR');

  // document.querySelector('#preferred-monitor').textContent = preferred_monitor;
  document.querySelector('#remembered-monitor').textContent = remembered_monitor;
  
} // showSavedPrefs


function getBooleanPref(section, setting = '', defVal = false ) {
  try {
    val = (!!setting ? !!parseInt(prefs['ZWIFT'][section][0][setting][0]) : !!parseInt(prefs['ZWIFT'][section][0]) )
  } 
  catch(err) { val = defVal }

  return val
}

function getNumberPref(section, setting = '', defVal = null ) {
  try {
    val = (!!setting ? prefs['ZWIFT'][section][0][setting][0] : prefs['ZWIFT'][section][0])
  } 
  catch(err) { val = defVal }

  return val
}

function getStringPref(section, setting = '', defVal = null ) {
  try {
    val = (!!setting ? prefs['ZWIFT'][section][0][setting][0] : prefs['ZWIFT'][section][0])
  } 
  catch(err) { val = defVal }

  return val
}

function rememberMonitor() {
  preferred_monitor = getNumberPref('CONFIG', 'PREFERRED_MONITOR')
  if (preferred_monitor) {
    if (!(savedprefs['saved'])) {
      savedprefs['saved'] = [];
    }
    if (!(savedprefs['saved'][0])) {
      savedprefs['saved'][0] = {};
    }
    if (!(savedprefs['saved'][0]['CONFIG'])) {
      savedprefs['saved'][0]['CONFIG'] = {};
    }
    savedprefs['saved'][0]['CONFIG']['PREFERRED_MONITOR'] = preferred_monitor;
    document.querySelector('#remembered-monitor').textContent = preferred_monitor;
  }
  // console.log(prefs);
}

function recallMonitor() {
  preferred_monitor = savedprefs['saved'][0]['CONFIG']['PREFERRED_MONITOR']
  if (preferred_monitor) {
    prefs['ZWIFT']['CONFIG'][0]['PREFERRED_MONITOR'] = preferred_monitor;
    document.querySelector('#preferred-monitor').textContent = preferred_monitor;
    }
}

function savePrefs () {
    world = document.querySelector('input[name="world"]:checked').value
    if (world == 0) {
      delete prefs['ZWIFT']['WORLD']
    } else {
      prefs['ZWIFT']['WORLD'] = world 
    }
    
    sport = document.querySelector('input[name="sport"]:checked').value
    prefs['ZWIFT']['SPORT'] = sport
    
    trainereffect = document.querySelector('#trainer-effect').value
    prefs['ZWIFT']['CONFIG'][0]['TRAINER_EFFECT'] = trainereffect


    neoroadfeel = document.querySelector('#roadfeel').checked
    prefs['ZWIFT']['CONFIG'][0]['NEOROADFEEL'] = (neoroadfeel ? 1 : 0)


    minimalui = document.querySelector('#minimal-ui').checked
    minimalleaderboards = document.querySelector('#minimal-leaderboards').checked
    fullscreen = document.querySelector('#fullscreen').checked 
    editinwatts = document.querySelector('#edit-in-watts').checked
    erg = document.querySelector('#use-erg').checked 
    powersmoothing = document.querySelector('#power-smoothing').checked 

    prefs['ZWIFT']['CONFIG'][0]['MINIMAL_UI'] = (minimalui ? 1 : 0)
    prefs['ZWIFT']['CONFIG'][0]['MINIMAL_LEADERBOARDS'] = (minimalleaderboards ? 1 : 0)
    prefs['ZWIFT']['CONFIG'][0]['FULLSCREEN'] = (fullscreen ? 1 : 0)
    prefs['ZWIFT']['WORKOUTS'][0]['EDIT_IN_WATTS'] = (editinwatts ? 1 : 0)
    prefs['ZWIFT']['WORKOUTS'][0]['USE_ERG'] = (erg ? 1 : 0)
    prefs['ZWIFT']['CONFIG'][0]['POWERSMOOTHING'] = (powersmoothing ? 1 : 0)

    var xml = builder.buildObject(prefs);
    // console.dir(xml)

    fs.writeFile(prefsxml, xml, (err) => {
      if (err) throw err;
      // console.log('It\'s saved!');
    });
} // savePrefs


function saveSavedPrefs () {

    var json = JSON.stringify(savedprefs);
    // console.dir(json)

    fs.writeFile(savedprefsjson, json, (err) => {
      if (err) throw err;
      // console.log('It\'s saved!');
    });
} // saveSavedPrefs



function renderWorldRoutes(worldRoutesNode, world, worldName, worldDisplayName, routes, sport) {

  // worldRoutesNode.outerHTML = '<ul class="choice-list list-group collapsed" id="routes-world-' + world + '-' + sport + '"><li class="list-group-header"><i class="fa fa-caret-square-o-down" aria-hidden="true"></i> ' + worldDisplayName + ' ' + sport + '</li></ul>'
  worldRoutesNode.outerHTML = `<ul class="choice-list ${sport.toLowerCase()} list-group collapsed" id="routes-world-${world}-${sport}"><li class="list-group-header"><i class="fa fa-caret-square-o-down" aria-hidden="true"></i> ${worldDisplayName} ${sport}</li></ul>`

  document.querySelector('#routes-world-' + world + '-' + sport + ' > .list-group-header').addEventListener('click', () => {
    target = document.querySelector('#routes-world-' + world + '-' + sport );
    target.classList.toggle("collapsed");
  })

   // console.log(stat);
  routes.forEach( (r) => {
    // console.log(r)
    if ((r.sports && r.sports[sport.toLowerCase()] && (!r.eventOnly)) || (r.sports === undefined)) {
      renderRoute(document.querySelector('#routes-world-' + world + '-' + sport ), world, r.name, r.signature, r.branchPreference, r.description, r.link, sport.toLowerCase())
    }
  })
  
}

function renderRoute(parentNode, world, name, signature, branchPreference, description, link, sport) {

    var l = document.createElement("li")
    parentNode.appendChild(l)

    var id = routeId(world, branchPreference, signature, sport)

    // l.outerHTML = '<li class="list-group-item" id="' + id + '"><div class=""><strong>' + name + '</strong><p>' + description.replace(/[\n]/g,'<br />')  + '</p></div></li>'
    l.outerHTML = '<li class="list-group-item" id="' + id + '"><div class=""><strong>' + name + '</strong><p></div></li>'

    if (isThisRouteInPrefs(world,branchPreference,signature, sport)) {
      document.querySelector('#' + id).className  = 'list-group-item selected'
    }

    document.querySelector('#' + id).addEventListener( 'click', (event) => {
      // alert(id)

      Array.from(parentNode.querySelectorAll('.list-group-item')).forEach( (c) => {
        c.className = 'list-group-item'
      })
      event.currentTarget.className = 'list-group-item selected'

      setPrefsRoute(world, branchPreference, signature, sport)

      showRouteDescription(world, name, description, link)
    })

}

function routeId(world, branchPreference, signature, sport) {
  switch (branchPreference) {
    case 2:
    case -1:
      return 'route-' + world + '-' + branchPreference
      break;
    default:
      return 'route-' + world + '-' + branchPreference + '-' + signature + '-' + sport
      break;
  }
}

function setPrefsRoute(world, branchPreference, signature, sport) {
  var prefix = ''
  var suffix = ''
  
  // switch (world) {
  //   case 2:
  //     prefix = 'RICHMOND_'
  //     break;
  //   case 3:
  //     prefix = 'LONDON_'
  //     break;
  //   // case 4:
  //   //   prefix = 'XYZ_'
  //   //   break;
  // }

  prefix = worldData[world].prefs.prefsPrefix

  if (sport == 'run' || sport == 'running') {
    suffix = '_RUN'
  }

  // Branch Preferences

  prefs['ZWIFT']['CONFIG'][0][prefix + 'BRANCH_PREFERENCE'] = branchPreference

  if (branchPreference == 6) {
    prefs['ZWIFT']['CONFIG'][0][prefix + 'ROUTE_SIGNATURE' + suffix] = signature
  }
  

}

function showRouteDescription(world, name, description, link) {
  
  var worldName = ''
  
  // switch (world) {
  //   case 1:
  //     worldName = 'Watopia'
  //     break;
  //   case 2:
  //     worldName = 'Richmond';
  //     break;
  //   case 3:
  //     worldName = 'London';
  //     break;
  //   // case 4:
  //   //   worldName = 'Xyz';
  //   //   break;
  // }

  worldName = worldData[world].worldDisplayName

  document.querySelector('#route-description').className = 'active'

  document.querySelector('#route-world').innerHTML = worldName;
  document.querySelector('#route-name').innerHTML = name;
  document.querySelector('#route-text').innerHTML = description.replace(/Route details:\n\n/g,'').replace(/[\n]/g,'<br />');
  if (link != '') {
    // document.querySelector('#route-link').innerHTML = 'More:<br /><a target="blank" href="' + link + '">' + link + '</a>';
    document.querySelector('#route-link').innerHTML = 'More:<br /><a href="#" onclick="require(\'opn\')(\'' + link + '\')">' + link + ' <i class="fa fa-external-link" aria-hidden="true"></i></a>';
  } else {
    document.querySelector('#route-link').innerHTML = '';
  }
}


function isThisRouteInPrefs(world, branchPreference, signature, sport) {
  var prefix = ''
  var suffix = ''

  // switch (world) {
  //   case 2:
  //     prefix = 'RICHMOND_'
  //     break;
  //   case 3:
  //     prefix = 'LONDON_'
  //     break;
  //   // case 4:
  //   //   prefix = 'XYZ_'
  //   //   break;
  // }

  prefix = worldData[world].prefs.prefsPrefix

  if (sport == 'run' || sport == 'running') {
    suffix = '_RUN'
  }

  if (branchPreference == 6) {
    return (getNumberPref('CONFIG', prefix + 'BRANCH_PREFERENCE') == branchPreference && getNumberPref('CONFIG', prefix + 'ROUTE_SIGNATURE' + suffix) == signature)
  } else {
    return (getNumberPref('CONFIG', prefix + 'BRANCH_PREFERENCE') == branchPreference)
  }


}


function activateWindowContent(id) {
    Array.from(document.querySelectorAll('.window-content')).forEach( (wc) => {
      wc.className = 'window-content'
    })

  document.querySelector(id).className = 'window-content active'

}